# Twitter Spaces

## **Mondays with Maya**

**#8:** [https://twitter.com/Maya\_Protocol/status/1632908951804157952](https://twitter.com/Maya\_Protocol/status/1632908951804157952)&#x20;

<details>

<summary>Key points</summary>

* Minting the first block, having all genesis nodes churn in, and opening up THORWallet and inbound addresses publicly for all to add funds will not happen simultaneously.

<!---->

*   Here's a more realistic timeline:

    \-Tuesday (07/03): Mainnet launched, and the first block minted.

    \-Wednesday (08/03): Sanity checks and internal testing.

    \-Thursday (09/03): Churn once the last Genesis Node reaches the Ethereum Network block tip.

    \-Friday (10/03): Significant liquidity can be added if everything looks good.
* MAYA allocation model is changing to be time-dependent instead of being dependent on the first inflows to pools. The first 75% of the award per pool will be given to people adding T1 liquidity from Friday, March 10th to Tuesday, 14th.

</details>

**#7:** [https://twitter.com/Maya\_Protocol/status/1630236344772448256](https://twitter.com/Maya\_Protocol/status/1630236344772448256)

**#6: Maya & Thorwallet** [https://twitter.com/Maya\_Protocol/status/1627700335836790787](https://twitter.com/Maya\_Protocol/status/1627700335836790787)

<details>

<summary>Key points</summary>

* THORWallet is currently in the last steps of testing. The Liquidity Auction is completely integrated into the mobile app and currently working on the previous stages of the web app integration.
* Halborn is currently working on the DASH bifröst audit. Our team prioritized this, and the final report will be available soon. Next on our list is [$AZTEC](https://twitter.com/search?q=%24AZTEC\&src=cashtag\_click), so stay tuned for more updates.
* **NEW:** Maya's cash-back program. Provide liquidity through a wallet and receive a link to share with friends. If they use your link to provide liquidity, you earn a percentage of their liquidity.

</details>

**#5: Maya & Halborn** [https://twitter.com/Maya\_Protocol/status/1625132975607418880](https://twitter.com/Maya\_Protocol/status/1625132975607418880)

<details>

<summary>Key points</summary>

* Halborn is giving Maya an audit of the code and a 360º analysis of new security measures in the market and economic security assessments.
* &#x20;Once Maya’s code audits are done, Aztec will be the next in line.&#x20;
* Maya’s first stage was to learn and research about THORChan: how and why it was implemented that way and how they Maya could manage to cover the same issues, and more. The actual stage consists in implementing.&#x20;

</details>

**#4:** [https://twitter.com/Maya\_Protocol/status/1622746989724110848](https://twitter.com/Maya\_Protocol/status/1622746989724110848)&#x20;

<details>

<summary>Key points</summary>

* The team is working on updating Maya to the TC version that supports savers, V101, and savers will be available at launch.
* Stage testing will be started when V101 is ready, and a stage liquidity auction will be conducted around Feb 20th.
* The THORWallet app has the tiers part of the liquidity auction integrated, and an affiliate program will be launched with THORWallet, rewarding $Maya tokens.
* There are plans to reward the first Tier 1 liquidity providers under 5M USD with 1% of $MAYA tokens to incentivize joining the LA early.
* Any wallet with a custom memo function can send funds to the LA, and every integrated chain affects the difficulty of integration.

</details>

**#3: Maya & Thorstarter** [https://twitter.com/Maya\_Protocol/status/1620210390641659912](https://twitter.com/Maya\_Protocol/status/1620210390641659912)&#x20;

<details>

<summary>Key points </summary>

* Thorwallet is looking for ways to incentivize people to participate in the Liquidity Auction, and more details will be shared later.
* Halborn Security has performed six security audits for Maya, and they are waiting for the final report to end the process.
* Aztec is coming soon, and CosmWasm and IBC will be enabled from the start for powerful composability.

</details>

**#2:** [https://twitter.com/Maya\_Protocol/status/1617673467599687680](https://twitter.com/Maya\_Protocol/status/1617673467599687680)

<details>

<summary>Key points </summary>

* Liquidity nodes have been improved by bonding LP units with profound intentions, resulting in higher yield, more people adding liquidity, more pool depth, and more affordability.
* Genesis nodes are motivated by a desire to protect users and are open to more than six nodes willing to put their reputations on the line.
* Economic incentives for nodes will be significant from the start, and genesis nodes will eventually be churned to make the system more secure and sustainable.

</details>

**#1:** [https://twitter.com/i/spaces/1dRKZMqNNqQxB?s=20](https://twitter.com/i/spaces/1dRKZMqNNqQxB?s=20)

<details>

<summary>Key points</summary>

* The latest Halborn Audits report shows no major vulnerabilities in Rune & Maya pools. Full report will be shared with the community in the following weeks.
* Supported tokens on the liquidity auction: $BTC, $RUNE, $ETH, and some stablecoins, with plans to add $KUJI, $Osmos, $BSC, and $BNB after the auction to reduce risks and increase security.
* The first three Maya Nodes will be run by @THORWalletDEX, @Thorstarter, and @Maya\_protocol, with an opportunity for organizations with a stake worth value to join the genesis nodes.

</details>

## Twitter Spaces with other hosts

**Cosmos Club & Aaluxx:** [https://twitter.com/CosmosClub\_/status/1622881702233141249](https://twitter.com/CosmosClub\_/status/1622881702233141249)&#x20;

**LPU & Maya:** [https://twitter.com/i/spaces/1OwxWwqXXaVxQ](https://twitter.com/i/spaces/1OwxWwqXXaVxQ)&#x20;

**AMA with Danku:** [https://twitter.com/Maya\_Protocol/status/1616118122137657345](https://twitter.com/Maya\_Protocol/status/1616118122137657345)

**THORWallet x Maya integration:** [https://twitter.com/THORWalletDEX/status/1597528829207392256](https://twitter.com/THORWalletDEX/status/1597528829207392256)&#x20;

**Liquidity Auction Tier ROI calculator with Kyle Krason:** [https://twitter.com/Maya\_Protocol/status/1630723295254548482](https://twitter.com/Maya\_Protocol/status/1630723295254548482) [\
](https://twitter.com/NACSTWEETS)
