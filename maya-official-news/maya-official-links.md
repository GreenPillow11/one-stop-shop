# Maya Official Links

![](../.gitbook/assets/laptop-chromebook-outline.svg)[Website](https://www.mayaprotocol.com/)

![](../.gitbook/assets/github.svg)[GitHub](https://github.com/Maya-Protocol)

![](../.gitbook/assets/file-type-gitlab.svg)[GitLab](https://gitlab.com/mayachain)
