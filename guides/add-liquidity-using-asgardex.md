# Add liquidity using Asgardex

1.  Create a Maya wallet address (see instructions [here](how-to-set-up-a-new-maya-wallet.md)). Ensure you have a new Maya wallet address to associate when adding liquidity. You can use the same Maya wallet for multiple pools (if different chains) but not the same Maya wallet address for multiple wallets of the same chain.&#x20;



    For example if you have two different Thorchain wallets you wish to use to add liquidity, then make sure you have two different Maya wallets to use.

{% hint style="info" %}
The rule: for each new wallet of any chain you’re using to add liquidity, use a different Maya wallet.
{% endhint %}

2. Open and log in to Asgardex. (alternate instructions if adding liquidity with Ledger on Asgardex [here](add-liquidity-using-ledger-on-asgardex.md))
3. Under the WALLET tab, choose ASSETS and select the appropriate row corresponding to the Ledger account you wish to provide from. Click the “Action” dropdown and choose the SEND option.

<figure><img src="../.gitbook/assets/Asgardex demo 2.jpeg" alt=""><figcaption></figcaption></figure>

3. Send Address field.

The Address field will use the same address for both PROVIDING or WITHDRAWING.

In the Address field, enter the correct address corresponding to the pool you're providing to.

{% hint style="danger" %}
ADDRESSES FOR MAINNET WILL CHANGE. THESE ARE EXAMPLES ONLY AND WILL BE REPLACED ON FRIDAY.
{% endhint %}

| RUNE | To be defined |
| ---- | ------------- |
| BTC  | To be defined |
| ETH  | To be defined |

4. Enter the Amount you wish to provide. Ensure that you have a little extra reserved for gas fees.

<figure><img src="../.gitbook/assets/Asgardex demo 3 - provide.jpeg" alt=""><figcaption></figcaption></figure>

4. The string entered into the Memo field will include the type of token, the pool being added to, your Maya wallet address, and the tier you wish to provide to. It has a specific format so please use the exact guide below.

Memo field text input (by a pool) when PROVIDING

A. Change out "XXX..." with your Maya wallet address

B. Change "TIERX" to whichever Tier you wish to provide to (eg. TIER1 TIER 2 or TIER3). PLEASE MAKE SURE TO USE CAPITAL LETTERS FOR THIS. If you leave this field empty it will automatically make you Tier 3.

C. Input as shown, make sure no spaces and include the "+:" at the beginning\


| RUNE | +:thor.rune:mayaXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX:TIERX |
| ---- | ------------------------------------------------------------- |
| BTC  | +:btc.btc:mayaXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX:TIERX   |
| ETH  | +:eth.eth:mayaXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX:TIERX   |

6. When it's all filled in, click the Send button. Enter your password to complete the tx.

\*It's recommended to use the “Fast” or “Fastest” option for send to ensure the tx doesn't get hung up over gas changes on the chain.&#x20;

When it's complete, check the tx to ensure it was successful.
