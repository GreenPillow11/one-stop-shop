---
description: Learn how to participate through the desktop and mobile version
---

# Add liquidity through THORWallet

### **Step-by-Step (Mobile App)**

Once your assets are available, and you have decided on your preferred[ Tier](https://assets.website-files.com/62a14669b65c6aeed054f32e/63e42e22564871477ebe7149\_Tabla%20MayaNuc.png), here are the steps you should follow:

1\. Download the ThorWallet application from either the[ App Store](https://apps.apple.com/ch/app/thorwallet-defi-wallet/id1592064324) or[ Google Play](https://play.google.com/store/apps/details?id=defisuisse.thorwallet). This should be pretty straightforward, but they also have a friendly[ "How to" guide](https://faqs.thorwallet.org/faqs/general/how-to-install-thorwallet-dex) if you want to use the Beta versions.

2\. [Create a wallet](how-to-set-up-a-new-maya-wallet.md) within the app or import one if you'd prefer. Their user interface guides you through both processes seamlessly, but they also have this[ Getting Started guide](https://faqs.thorwallet.org/faqs/general/getting-started-guide) if you need help. If you do generate a new wallet, you will need to write down your new seed phrase somewhere safe. Remember, this is all non-custodial!

<figure><img src="https://lh5.googleusercontent.com/7_u1ai1n1gaaOVDsX5Xrd_A7j_6ZVJl5JP2iKDR1MWG4o-CBe7jLC-GrzpZ-RXHae6NCKmNsetskp9hcNKiI-xZSFhgs-cOZbybu8wCmpj4q1JvRViTc7EBlUTZGS7TyXVH2BCme1oNMM4P2rVd5H3P4HyTHNuFGwOV0_nmRY0kAHy6gFDivRPmi9qjpow" alt=""><figcaption></figcaption></figure>

3\. Explore the app and get used to it. You can send a small amount of crypto to your new addresses to ensure everything works fine.

4\. Once ready, send your assets to their corresponding address. You can use their QR code representation if it is convenient. Click on any asset to find the "receive" button, and the QR code will appear. Send only native BTC, ETH, RUNE, and ERC-20 USDC or USDT; you can't provide wrapped or bridged tokens into Maya.

<figure><img src="../.gitbook/assets/Captura de Pantalla 2023-03-06 a la(s) 13.10.01.png" alt=""><figcaption></figcaption></figure>

5\. Tap on the Wallet icon on the bottom navigation menu, and select the Maya Liquidity Auction banner from those on the top.&#x20;

<figure><img src="https://lh4.googleusercontent.com/4Kv_U0b9XuNFRiw-vYQli4qg6aahwBfS1DdKQuSRW1KiytAWwesYhr4Vt5f7daZA7HhcU0yg8kv6Grx7CR0bfUZIgQT49n8FnvLtfwOeHxF74LdcryQl1TQElkp1b3mS9fuqP3cb3IgEVvE-6WK6fosDLDobeR4RmIMOJQHBiONuxl3IRe5zdAf3s2ShLA" alt=""><figcaption></figcaption></figure>

&#x20;

6\. Select your preferred asset from the list. Click continue.

<figure><img src="https://lh4.googleusercontent.com/4Kv_U0b9XuNFRiw-vYQli4qg6aahwBfS1DdKQuSRW1KiytAWwesYhr4Vt5f7daZA7HhcU0yg8kv6Grx7CR0bfUZIgQT49n8FnvLtfwOeHxF74LdcryQl1TQElkp1b3mS9fuqP3cb3IgEVvE-6WK6fosDLDobeR4RmIMOJQHBiONuxl3IRe5zdAf3s2ShLA" alt=""><figcaption></figcaption></figure>

7\. Enter the amount you wish to provide to the Maya liquidity pools and select your Tier. Tier 3 is selected by default.

<figure><img src="https://lh6.googleusercontent.com/et7xAYDNYniEf3n2Fn-piTqjLJSuGA6s6dBjO-BZ0WRAH5576bqLG2hmrOmT-1e6Tb7gfltHw4QDA_fSdduI3OO117GGpo3R_orplOvrtI2JJYSGr6AOw87ldOhncfYwk4xoPBEVdzcEnVJdX82xR9Z7yNrV1zKPnWs8JHX9rfCxHj38-As4W5IocBYzEQ" alt=""><figcaption></figcaption></figure>

&#x20;

8\. In the next screen, only if you are prompted, approve the necessary ERC-20 contract and wait for confirmation.

<figure><img src="https://lh5.googleusercontent.com/PlN4fNZRL56acOCUB7PdvfZk9CN6pvrCPOjZdxOnG8p5iX8j1OyqosC8WAnFht4OdNM_V2tJKZGvbOwkEfberATeqoFqqHm6hQrNfeb3NqngxtFcYyAtTBO8n52CQY5ATzp7cG8z5sJFseZJy_qE3sCEeoHW1a5ngmFQ3kJGnBgLKLHuiiwaHKrMi4Uwag" alt=""><figcaption></figcaption></figure>

9\. Click the Add Liquidity Now button to broadcast the transaction and await the proper confirmation. You are all set!

<figure><img src="https://lh6.googleusercontent.com/0HeBTIN7DaUeLlEeSJFdC0LXyEKwq3ns_IhHCxwY-GulrCl6TNbPZpBFMnwTmkwgYbqFlnEDqyXxW5x9cZp8cllvYR6wVLMUOkeCdmBCweQBMIlJSJkd07UB04EjFyj5NdaX3VcfcFbc7UmwVjP0kHKEVlTB_2apXO8NDeqHk1SS63g4Uu0Jd8szSsSy_w" alt=""><figcaption></figcaption></figure>

### ThorWallet Step by Step (Web App)

Once your assets are available, and you have decided on your preferred[ Tier](https://assets.website-files.com/62a14669b65c6aeed054f32e/63e42e22564871477ebe7149\_Tabla%20MayaNuc.png), here are the steps you should follow:

1. Go to the official [ThorWallet website](https://www.thorwallet.org/) and click on the Enter the App green button.

<figure><img src="https://lh3.googleusercontent.com/liXsrleClgbsS4cXJHwrdbNwBNf81TbucpJRezJ5qOZZD_dvZgI9FyuCBxqXuRd3v__cRNzKqP31_jDLjuGb_CzDWV5m6QQbPejlzHvYIXE2L5xz_2H9Q2F0IMpRWhDsX5PK9jYn0M7KG5koZJtrRPc" alt=""><figcaption></figcaption></figure>

2. Connect, import, or create a wallet by clicking on the “connect” button on the upper-right part of the screen. If you do generate a new wallet, you will need to write down your new seed phrase somewhere safe. Remember, this is all non-custodial!

<figure><img src="../.gitbook/assets/Captura de Pantalla 2023-03-06 a la(s) 16.32.03.png" alt=""><figcaption></figcaption></figure>

Explore the app and get used to it. Visit ThorWallet’s [help center](https://faqs.thorwallet.org/faqs/wallet) if you need it. Send a small amount of crypto to your new addresses to ensure everything works fine.

3. Once ready, fund your ThorWallet address with the assets you intend to provide as liquidity. Send only native BTC, ETH, RUNE, and ERC-20 USDC or USDT; you can't provide wrapped or bridged tokens into Maya.

<figure><img src="../.gitbook/assets/Captura de Pantalla 2023-03-06 a la(s) 16.35.40.png" alt=""><figcaption></figcaption></figure>

3. Look for the “Earn” section on the left part of the screen, and click on “Liquidity Pools.” Select Maya from the filter buttons.

<figure><img src="../.gitbook/assets/Captura de Pantalla 2023-03-06 a la(s) 16.41.55.png" alt=""><figcaption></figcaption></figure>

3. Select the pool you wish to add liquidity into. Input the amount you’d like to use and select your desired Tier. (Tier 3 is selected by default).

<figure><img src="../.gitbook/assets/Captura de Pantalla 2023-03-06 a la(s) 16.43.06.png" alt=""><figcaption></figcaption></figure>

3. Click on the “preview” and/or the “Add Liquidity now” buttons. Unlock your wallet with your password if you are asked to finalize the transaction.
4. Wait for the proper confirmation, and you are all set!

<figure><img src="../.gitbook/assets/Captura de Pantalla 2023-03-06 a la(s) 16.46.16.png" alt=""><figcaption></figcaption></figure>

### THORWallet Step-by-Step Videos

If you prefer video format, we have prepared two step-by-step videos for you:

1. [ThorWallet Step-by-Step Mobile App Video.](https://www.youtube.com/watch?v=uavMxAT2yM0)
2. [ThorWallet Step-by-Step Web Application Video.](https://youtu.be/AEtI2bpvatA)
