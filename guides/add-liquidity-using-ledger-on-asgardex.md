# Add liquidity using Ledger on Asgardex

Instruction to connect Ledger to Thorswap app can be found[ here](https://github.com/thorchain/ledger-thorchain/blob/main/docs/INSTRUCTIONS.md). The same functional steps apply to providing liquidity via Thorswap or Asgardex.

**1. Setup**

* Plug in your Ledger device, unlock it, and open the chain app you wish to use in Asgardex. We’ll use the THORChain app as an example, but you may be adding one of the other Ledger accounts like Ethereum or Bitcoin, to use in the auction.
* On your computer, open the ASGARDEX app.
* If you haven't already set up a software wallet on the first run, follow the prompts to do that first.
* Click on WALLET, then SETTINGS, find the chain account you wish to add, then click ADD LEDGER

If you see an error such as “No Device Found” your Ledger device may be locked or not have the Thorchain app (or your choice) on Ledger open. Troubleshoot and try again.

\*Note: When initially launching the Thorchain app on Ledger, you may see a “Pending Ledger review.” You may click past this screen until it to the “THORChain ready” screen.

After successfully adding the Ledger, it will appear below the software wallet account. You may have two of the same types of tokens with one now marked “Ledger”

**2. Verify Address**

For maximum security, you should verify the address on the Ledger device before receiving funds. This ensures ASGARDEX is displaying the correct address.

**3. Check Balance**

Checking balance can be accomplished via the WALLET under the ASSETS tab.

**4. Providing to the Liquidity Auction**&#x20;

Under the WALLET tab, choose ASSETS and select the appropriate row corresponding to the Ledger account to wish to provide from.

Click the “Action” dropdown and choose the SEND option.

**5. Send Address field**

The Address field will use the same address for both PROVIDING or WITHDRAWING

In the Address field enter the correct address corresponding to the pool you're providing to.

{% hint style="danger" %}
ADDRESSES FOR MAINNET WILL CHANGE. THESE ARE EXAMPLES ONLY AND WILL BE REPLACED ON FRIDAY.
{% endhint %}

| RUNE | To be defined |
| ---- | ------------- |
| BTC  | To be defined |
| ETH  | To be defined |

**6. Amount field**

Enter the Amount you wish to provide. Ensure that you have a little extra reserved for gas fees.

**7. Memo field**&#x20;

The string entered into the Memo field will include the type of token, the pool being added to, your Maya wallet address, and the tier you wish to provide to. It has a specific format so please use the exact guide below.

Memo field text input (by pool) when PROVIDING

A. Change out "XXX..." with your Maya wallet address

B. Change "TIERX" to whichever Tier you wish to provide to (eg. TIER1 TIER 2 or TIER3). PLEASE MAKE SURE TO USE CAPITAL LETTERS FOR THIS. If you leave this field empty it will automatically make you Tier 3.

C. Input as shown, make sure no spaces and include the "+:" at the beginning.\


| RUNE | +:thor.rune:mayaXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX:TIERX |
| ---- | ------------------------------------------------------------- |
| BTC  | +:btc.btc:mayaXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX:TIERX   |
| ETH  | +:eth.eth:mayaXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX:TIERX   |

**8. Complete tx**&#x20;

When it's all filled in, click the Send button. You will need to confirm the transaction on your Ledger for it to be complete.&#x20;

\*Ledger users: Make sure you’re logged into your Ledger before hitting Send.&#x20;

Both Thorswap/Asgardex: After hitting Send you need to click through several screens reviewing the tx on the Ledger device to arrive at your “Confirm” screen to allow the transaction. If you don’t confirm, the transaction will not be complete.

\*\*Asgardex recommendation: Use the “Fast” or “Fastest” option for sending to ensure the tx doesn't get hung up over rising gas costs trying to complete during on-chain validation.

When it's complete, check the tx to ensure it was successful!\
