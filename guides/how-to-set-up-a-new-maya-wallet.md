# How to set up a new Maya wallet

Only one THORChain wallet per Maya wallet should be used to add liquidity to the Liquidity Auction.

{% hint style="info" %}
Maya Address not available yet on THORWallet. Expected to be live on Wed, 15 March 10am CST
{% endhint %}

1\. Go to [THORWallet DEX ](https://app-alpha.thorwallet.org/)and click "Connect."

<figure><img src="../.gitbook/assets/Maya wallet demo 1.jpg" alt=""><figcaption></figcaption></figure>

2\. Accept the TOS and Privacy Policy.

3\. Select "Create Keystore."

<figure><img src="../.gitbook/assets/Maya wallet demo 3.jpg" alt=""><figcaption></figcaption></figure>

4\. Create and confirm a password for the keystore file. Make sure to save this password. You'll need it to open the keystore file.

<figure><img src="../.gitbook/assets/Maya wallet demo 5.jpg" alt=""><figcaption></figcaption></figure>

5\. Click "Download" and save the thorwallet-keystore.txt keystore file somewhere safe on your local drive.

6\. On Connect screen select "Keystore" option to connect using your newly created keystore file.

7\. Select the new thorwallet-keystore.txt keystore file saved on your drive, input your password and click "Unlock."

8\. Select "My Assets" from the left sidebar.

<figure><img src="../.gitbook/assets/Maya wallet demo 10.jpg" alt=""><figcaption></figcaption></figure>

9\. Search for MAYA token in "Search assets" at the top.

10\. Select MAYA.

<figure><img src="../.gitbook/assets/Maya wallet demo 11.jpg" alt=""><figcaption></figcaption></figure>

11\. Change the module function to "Receive" - there, you will be able to copy your new Maya wallet address. It should begin with "maya..."

<figure><img src="../.gitbook/assets/Maya wallet demo 12.jpg" alt=""><figcaption></figcaption></figure>

{% hint style="info" %}
Save your new Maya wallet address, and keystore (if you created one), and password in a safe place. You’ll need your Maya address for the next steps.&#x20;

In the example, the Maya wallet created with the keystore file was:

maya1668cguves6x7sfm08pkm2u78exwaq63t9kqwse
{% endhint %}

\
