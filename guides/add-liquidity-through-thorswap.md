# Add liquidity through THORSwap

1. Set up a wallet by creating a keystore file on THORWallet to create a Maya address.
2. Click the button "Send" in the left column.&#x20;
3. Select your asset (RUNE, BTC, ETH) and enter the amount you want to add.
4.  The Address field will use the same address for both PROVIDING or WITHDRAWING.

    In the Address field, enter the correct address corresponding to the pool you're providing to.

{% hint style="danger" %}
ADDRESSES FOR MAINNET WILL CHANGE. THESE ARE EXAMPLES ONLY AND WILL BE REPLACED ON FRIDAY.
{% endhint %}

| RUNE | To be defined  |
| ---- | -------------- |
| BTC  | To be defined  |
| ETH  | To be defined  |

5. The string entered into the Memo field will include the type of token, the pool being added to, your Maya wallet address, and the tier you wish to provide. It has a specific format, so please use the exact guide below.

Memo field text input (by a pool) when PROVIDING

A. Change out "XXX..." with your Maya wallet address

B. Change "TIERX" to whichever Tier you wish to provide to (eg. TIER1 TIER 2 or TIER3). PLEASE MAKE SURE TO USE CAPITAL LETTERS FOR THIS. If you leave this field empty, it will automatically make you Tier 3.

C. Input as shown. Make sure no spaces and include "add:" at the beginning

| RUNE | add:thor.rune:mayaXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX:TIERX |
| ---- | --------------------------------------------------------------- |
| BTC  | add:btc.btc:mayaXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX:TIERX   |
| ETH  | add:eth.eth:mayaXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX:TIERX   |

<figure><img src="../.gitbook/assets/Captura de Pantalla 2023-03-06 a la(s) 21.17.48.png" alt=""><figcaption></figcaption></figure>

6. When it's all filled in, click the Send button. Enter your password to complete the tx.

<figure><img src="../.gitbook/assets/Captura de Pantalla 2023-03-06 a la(s) 21.19.16.png" alt=""><figcaption></figcaption></figure>
