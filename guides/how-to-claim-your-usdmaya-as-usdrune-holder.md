# How to claim your $MAYA as $RUNE holder

$RUNE holders are eligible for MAYA airdrop, as outlined [here](../maya-whitepaper-2.0/usdmaya-token/technical-overview-mt.md).&#x20;

Even if you send 1 $RUNE to the LA and get refunded due to your MAYA address already being linked to another account, your transaction will still be counted. Essentially, all addresses that attempt to send money to the vault, even if they get the memo wrong, are refunded, or can't add due to having another wallet, will be visible.

Each eligible RUNE address must be linked to a MAYA address for the airdrop from as soon as Maya Mainnet is live on THORWallet up until April 17th at 23:59:59 UTC.

1. **Create a MAYA address (options to do so listed below):**

a. [THORWallet DEX mobile wallet App](add-liquidity-through-thorwallet.md) (both iOS or Android) will automatically create a MAYA address. Do ensure your THORWallet DEX App is updated to the latest version.

b. THORWallet DEX webapp with a keystore will automatically create a MAYA address.&#x20;

c. mayanode cli

Note: MAYA address will have the format of mayaxxxxxxxxxxxxx

2. **Link Rune address to Maya address (multiple options listed below):**

The last and most important step, in order to claim the $MAYA tokens you must send at least 1 $RUNE to the Maya pool:

a. Using THORWallet DEX mobile wallet App (both iOS or Android) or webapp with Keystore. Check "[Add liquidity through THORWallet](add-liquidity-through-thorwallet.md)” for instructions. Depositing a symmetrical LP with THORWallet DEX (after the Liquidity Auction) will be very similar.

b. Using THORWallet Dex webapp with XDEFI

c. Using Asgardex

d. Using mayacode cli

{% hint style="info" %}
Note: You do not have to keep your deposit in the Liquidity Auction/Pools for any extended period of time. Once the deposit has been successful, your RUNE-MAYA addresses will be linked, and you can then choose to withdraw the liquidity anytime.
{% endhint %}

{% hint style="danger" %}
If you want to add more than 1 $RUNE to the LA make sure you do that with the first address (**A**), if you do that with the second address (**B**) those $RUNE will be refunded and will not participate in the Liquidity Auction. The rest will be ONLY LINKED for the purpose of the airdrop.&#x20;



Here’s what would happen If you link 2 THORChain addresses (**A** and **B**) with MayaChain address (**M**):



1. First, you send 1 $RUNE from address **A** to CACAO/RUNE pool with the address of **M** in memo to link address **A** and **M**.
2. Second, you send 1 $RUNE from address **B** to CACAO/RUNE pool with the address of **M** in memo to link address **B** and **M**.



**After the above steps, address A will be considered a live LA deposit, and address B will be linked for the purpose of a $MAYA airdrop.** However, only one **M** address can be linked to each external asset address for LA specifically, and therefore, address **B** will no longer be able to enter LA successfully if address **A** is already linked to **M**.



If you mistakenly add 1 $RUNE with each address and both get linked, the first one will be allowed as an actual LP, and the rest will be refunded as an error. Regardless of the case, enough information will be available to find you, count your RUNE, and send your $MAYA to one consolidated Maya address.&#x20;
{% endhint %}



3. **Users holding Rune in other wallets (e.g. Ledger, Trust Wallet, etc).**

a. You must first generate a MAYA wallet, as per Step 1.

b. If you do not mind inserting your seed words into THORWallet DEX, you can then follow Step 2 above. This is strongly discouraged for hardware wallet seed words.

c. Otherwise, you must manually construct a transaction memo in a custom memo field in Asgardex or Thorswap.

4. **Receive $MAYA airdrop**

After completing the first two steps above, the MAYA tokens will be automatically airdropped into your MAYA address before April 26th based on the linked RUNE addresses and their respective weights based on the airdrop criteria.

### Claiming Using Manual Transaction Memos (e.g. Asgardex)&#x20;

{% hint style="info" %}
Note: This option is only for those with advanced knowledge of MAYA Vault addresses, including churning details during/after the Liquidity Auction. Be careful while following this method.
{% endhint %}

1. Need 1 RUNE at least to send to the Maya TC/RUNE Asgard address (address format thor1xxxxxxxx).
2. Memo will be like this: +:thor.rune:maya1xxxxxxxxxxx, where maya1xxxxxxxxx is the user’s MAYA address.
3. Make sure you use SEND, not DEPOSIT.&#x20;
4. After sending this memo, the linking process is finished.

{% hint style="info" %}
Note: &#x20;

1\. Maya TC/RUNE Asgard address will have changed if there has been a churn in the MAYA Protocol during/after the Liquidity Auction period. So, the person who’s following this needs to be very knowledgeable about the details of the MAYA churning and its updates.&#x20;

2\. If the 1 RUNE is sent to the earlier vault, it will not be read by Maya Nodes, the RUNE will be lost, and the airdrop won’t apply.
{% endhint %}

\
