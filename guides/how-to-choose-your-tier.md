# How to choose your tier

Users who want to participate in the Liquidity Auction must select 1 of the 3 Tiers available to allocate their external assets.&#x20;

This Liquidity Auction Withdrawal Tiers model below prevents rapid withdrawals right after the $CACAO distribution occurs and subsequent lockup period expirations. Do note that you can only select 1 Tier per wallet; the longest lockup Tier will be considered. The nominal distribution of $CACAO to each of the Tiers depends on how many participants enter into different tiers and the number of assets in each. \


<figure><img src="https://lh3.googleusercontent.com/Cv4XbFh369_-0elS5XZ8UHT4J3iwqQYkcF8LuQp9hPXB91nezKeQNqzUPiblv1Ufo7fTO4qgdX1PjFS-Zgg4blG4iGwf43zUoYu6kidgrCbk7fW82tB62g8CKXoSXcqKsZtex7C68M5U8_Vz0ObbQ-s" alt=""><figcaption></figcaption></figure>

### Tier 1

* Tier 1 has the longest lockup period of 200 days, along with a 0.5% limit to daily withdrawal. This tier incentivizes more long-term liquidity providers into the initial bootstrap of the protocol. These users are rewarded with a higher LP share, potential for 2x or more, and an additional $MAYA airdrop. Tier 1 Liquidity Providers (LPs) are our most committed long-term LPs; therefore, withdrawals are not permitted during the Liquidity Auction period of 21 days.&#x20;
* Risk Reward:&#x20;

\-Highest risk and reward potential&#x20;

\-There is always risk through market volatility

### Tier 2

* Tier 2 has a medium lockup period of 90 days with a 1.5% limit to daily withdrawal limits. These users are estimated to have a return of about 1.8x. Withdrawals during the Liquidity Auction period will only be allowed for Tier 2 & Tier 3 LPs without any limits. They can exit anytime if they no longer want to participate in the auction.
* Risk Reward:

\-Medium risk and reward potential

### &#x20;Tier 3

* Tier 3 has the shortest lockup duration of 30 days and a 4.5% limit to daily withdrawal limits. These users are estimated to have a return of about 1.34x. Withdrawals during the Liquidity Auction period will only be allowed for Tier 2 & Tier 3 LPs without any limits. They can exit anytime if they no longer want to participate in the auction.
* Risk Reward:&#x20;

\-Lowest risk and reward potential

{% hint style="warning" %}
Importantly, **for Tier 2 and Tier 3, withdrawing** from the Liquidity Auction is **all or nothing.** You cannot just withdraw 50% or some such.
{% endhint %}

### Which Tier is the best strategy?&#x20;

* The “best” strategy depends completely on your risk and reward appetite. That said, Liquidity providers with longer-term horizons may choose Tier 1, as this provides the highest reward heavily at the expense of shorter-term players like Tier 2 and 3. These extra rewards incentives deepen liquidity on a longer-term basis, giving the network more time to accrue liquidity and reach stability, in turn creating better cross-chain swaps for users.&#x20;
* The best strategy for aspiring node operators is to participate as a Tier 1 LP, given that they theoretically have a long-term commitment already. This would result in a better chance of having more Liquidity for potential Liquidity Bond Wars to be churned into the active validator set.&#x20;
* CACAO: Asset prices will likely change after the donation event, as Maya Protocol will be live for swapping and arbing. Thus, all liquidity positions will likely have some amount of Impermanent Loss (IL). Since Maya’s IL Protection (ILP) has a 50-day cliff + 100/400-day ramp, early withdrawals may face some of this IL.

Check out a great [calculator](https://cdn.discordapp.com/attachments/926331975160168448/1080287019403575376/MAYA\_Liquidity\_Auction\_Calculator.xlsx) created by Kyle Krason for the Return on Investment expectations depending upon the number of external assets allocated to each of the different Tiers during the Liquidity Auction.&#x20;

<figure><img src="https://lh5.googleusercontent.com/HKOu6fmMJjIfk_soGoF-9yn1X3T6mtKIFwbWfsx-QCA-xk3pm9Nr7INomj0xaMUu6n7ddD9hbc_h3uWtrpCq4vRfFwfDfUUBpaVFJ4-cNSJk84hTYgUgycwSPgJa_MuDnA0uo8oLV_1oVDnN3dlctu8" alt=""><figcaption></figcaption></figure>

Aaluxx and Kyle also had a brief [discussion](https://twitter.com/Maya\_Protocol/status/1630723295254548482) about the spreadsheet created and the different Tier risks and rewards.

Please do your own research on the Tiers and know that the Liquidity Auction is not without risks. If the Liquidity Auction is deemed disadvantageous for any reason (for example, there was too little liquidity raised) and the community decides to undo everything via Ragnarok, Tier 2 and 3 LPs might end up receiving back less than they originally deposited and effectively take a loss.

LPs that prefer not to have these types of risks or withdrawal limits can wait for the Liquidity Auction to be over and manually acquire $CACAO from a pool to add liquidity then. Wallets funded this way will have no limits or lockups.

\
If you have any questions about the Tiers, please be sure to join the Discord and ask us questions: [https://discord.gg/mayaprotocol](https://discord.gg/mayaprotocol)
