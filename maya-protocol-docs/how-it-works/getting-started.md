---
description: Getting started with MAYAChain and How Tos
---

# Getting Started

## Using MAYAChain

Users do not need CACAO to interact with MAYAChain. They can perform swaps and add/remove liquidity without ever directly touching CACAO or the MAYAChain protocol. These users need to find a MAYAChain-connected wallet.

However, more advanced users will want to add symmetrical liquidity or bond as a Node. For this, they will need CACAO.

### Acquiring Native Cacao

There are several ways.

* Buy any MAYAChain supported assets (such as BUSD/BTC/ETH) then swap it for native CACAO.

From there, send it to a [self custody wallet](https://linen.app/articles/what-is-a-self-custody-non-custodial-wallet/).

## **What Wallets Support Native CACAO?**

Many wallets support Native CACAO. See [below ](getting-started.md#wallets-and-interfaces)for more information.

![Supported Wallets](../.gitbook/assets/image%20\(3\).png)

## Adding and Removing Liquidity

### Entering and Leaving a Pool

To deposit assets on MAYAChain, you need a compatible wallet with your assets connected to one of the many User Interfaces. Liquidity providers can add liquidity to any of the active or pending pools. There is no minimum deposit amount, however, your deposit will have to cover the deposit and later a withdrawal fee costs. The ability to manage and withdraw assets is completely non-custodial and does not require any KYC or permission process. Only the original depositor has the ability to withdraw them (based on the address used to deposit the assets). Note, every time you add liquidity, Impermanent Loss Protection time resets.

While Symmetrically additions are recommended, Asymmetrical additions are supported, below are the rules:

If you add symmetrically first;

* You will be able to add asymmetrically with CACAO later
* You will be able to add asymmetrically with ASSET later but it would create a new LP position
* You will be able to add symmetrically later

If you add asymmetrically with ASSET first;

* You will be able to add asymmetrically with CACAO later but it would create a new LP position
* You will be able to add asymmetrically with ASSET later
* You will be able to add symmetrically later but it would create a new LP position

If you add asymmetrically with CACAO first:

* You will be able to add asymmetrically with CACAO later
* You will be able to add asymmetrically with ASSET later but it would create a new LP position
* You will not be able to add symmetrically later

![Addition Rules](https://lh3.googleusercontent.com/Vqi0wC-1dEnTGS410rXaiKpaGW5KUrzEBZPtD\_jPyWOKsooVQtWZ5hZlJWuAvmuA4c22V4WGjjlDGKKhE6p4JWKXzHKt5CS4tvnKDGdNuTsEpkQr7Ual0LpMWkEH1yFIzCqzC\_Do)

### Withdrawing Liquidity

Liquidity providers can withdraw their assets at any time and without any cooldown period, aside from the confirmation time. The network processes their request and the liquidity provider receives their ownership percentage of the pool along with the assets they’ve earned. Fees do apply when withdrawing, see [Outbound Fee](fees.md#outbound-fee).

## **How to track your position**

You can see your position if you connect to MAYAChain via an Interface you can use [MAYAYield](https://app.thoryield.com/).

➜ [MAYAYield Guide](https://thorswap.medium.com/introducing-thoryield-v2-%EF%B8%8F-a6618c1cfcdb)

**There are 3 factors affecting returns:**

* **Proportion of transaction volume to pool depth** — If there is high volume compared to the depth of the pool then there will be higher rewards available to liquidity providers. If there is low volume compared to the depth of the pool then there will be lower rewards available.
* **Share of the pool** — If you have a large share of the pool, you’ll earn higher returns. If a liquidity provider has 1% of a pool they receive 1% of the rewards for that pool.
* **Fee size** — fees are determined by the underlying blockchain and the rewards from fees are proportional to the fees charged. A chain with higher fees will generate higher rewards for liquidity providers.

\*Of significant note is that this mechanism of providing liquidity into the protocol; creates an opportunity for holders of non-yield generating assets (e.g. BTC, BNB) to earn a return on their investments.

## Wallets and Interfaces

We are working with www.thorwallet.org and @ElDoradoMKT to have viable and easy to use interfaces for you to add liquidity and execute swaps! Be on the lookout for updates from them.&#x20;
