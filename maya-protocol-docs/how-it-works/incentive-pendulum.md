---
description: MAYAChain's Incentive Pendulum keeps the network in a balanced state.
---

# Incentive Curve

The capital on MAYAChain can lose its balance over time. Sometimes there will be too much capital in liquidity pools; sometimes there will be too much bonded by nodes. If there is too much capital in liquidity pools, the network is unsafe. If there is too much capital bonded by nodes, the network is inefficient.

If the network becomes unsafe, it increases rewards (block rewards and liquidity fees) for node operators and reduces rewards for liquidity providers. If the network becomes inefficient, it boosts rewards for liquidity providers and reduces rewards for node operators.

## Balancing System States

MAYAChain can be in 1 of 5 main states—

* Unsafe
* Under-Bonded
* Optimal
* Over-Bonded
* Inefficient

These different states can be seen in the relationship between bonded Cacao and pooled Cacao. The amount of Cacao which has been bonded by node operators, and the amount which has been added to liquidity pools by liquidity providers.

### Optimal State

![](../.gitbook/assets/optimal.jpg)

In the optimal state, bonded LP is roughly 85% of the liquidity, with the remaining being simply pooled LP. Both Bonded and Pooled LP is 50% Cacao and 50% assets. This is the desired state. The system makes no changes to the incentives for node operators or liquidity providers.

### Unsafe State

The system may become unsafe. In this case, pooled capital is more than 1/4 bonded capital, a 75-25 split.&#x20;

This is undesirable because it means that it's become profitable for node operators to work together to steal assets.

To fix this, the system increases the amount of rewards going to node operators and lowers the rewards going to liquidity providers. This leads to more node operators getting involved, bonding more LP and increasing bonded capital. This also disincentivises liquidity providers from taking part. They receive less return on their investment, so they pull assets out and reduce the amount of pooled capital. With time, this restores balance and the system moves back towards the optimal state.

### Inefficient State?&#x20;

In Maya, the system can never become inefficient, since all of the capital is always at work in LP. Still, the network wishes to incentivize fresh liquidity to leverage the protocol and increase the flywheel effect of liquidity.&#x20;

To do this this, the system increases rewards for liquidity providers and decreases rewards for node operators. This attracts more liquidity providers to the system. Liquidity providers add more capital to receive more rewards, increasing pooled capital.&#x20;

Bonded capital falls as a percentage of total liquidity.\
In this way, the system returns to the optimal state.

### Under and Over-Bonded States

The under- and over-bonded states are less severe intermediary states. Being under-bonded is not a threat in itself because it is not yet profitable for node operators to steal. Being over-bonded is not a problem in itself because the system is still operating quite well.

The MAYAChain team does not expect the unsafe or inefficient states to come up often. The system will be near the optimal state most of the time, particularly as it gets easier for people to run node.

You can read more about this in our Whitepaper, in the section about [Liquidity Nodes.](../../maya-whitepaper-2.0/liquidity-nodes/)
