---
description: Learn how MAYAChain Works
---

# How It Works

### High Level Overview

{% content-ref url="understanding-mayachain.md" %}
[understanding-mayachain.md](understanding-mayachain.md)
{% endcontent-ref %}

### Getting Started

{% content-ref url="getting-started.md" %}
[getting-started.md](getting-started.md)
{% endcontent-ref %}

### Technical Info

{% content-ref url="technology.md" %}
[technology.md](technology.md)
{% endcontent-ref %}

{% content-ref url="incentive-pendulum.md" %}
[incentive-pendulum.md](incentive-pendulum.md)
{% endcontent-ref %}

{% content-ref url="broken-reference" %}
[Broken link](broken-reference)
{% endcontent-ref %}

{% content-ref url="fees.md" %}
[fees.md](fees.md)
{% endcontent-ref %}

{% content-ref url="governance.md" %}
[governance.md](governance.md)
{% endcontent-ref %}

{% content-ref url="constants-and-mimir.md" %}
[constants-and-mimir.md](constants-and-mimir.md)
{% endcontent-ref %}

{% content-ref url="security.md" %}
[security.md](security.md)
{% endcontent-ref %}

{% content-ref url="mayachain-name-service.md" %}
[mayachain-name-service.md](mayachain-name-service.md)
{% endcontent-ref %}
