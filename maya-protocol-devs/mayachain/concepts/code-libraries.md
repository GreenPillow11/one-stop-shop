---
description: The following libraries will help you in your integration
---

# Code Libraries

### XCHAINJS

Started as a wallet library for creating keystores, getting balances and history, as well as signing and broadcasting transactions but has now expanded as a one-stop shop for THORChain functionality. \
[https://docs.xchainjs.org/overview/](https://docs.xchainjs.org/overview/)\
[https://github.com/xchainjs/xchainjs-lib](https://github.com/xchainjs/xchainjs-lib)

### XCHAINPY

XCHAINJS ported to python.

[https://github.com/xchainjs/xchainpy-lib](https://github.com/xchainjs/xchainpy-lib)

### XChainDart

XChainJs ported to Dart

[https://github.com/dragonsdex/xchaindart](https://github.com/dragonsdex/xchaindart)

### THORCHAIN-IOS

iOS library built in swift for connecting to THORChain and getting the right transaction details.

[https://github.com/thorchain/thorchain-ios](https://github.com/thorchain/thorchain-ios)

### ASGARDEX-PACKAGES

Libraries for connecting to Midgard, as well as computing swap outputs, fees and more.\
[https://gitlab.com/thorchain/asgardex-common/asgardex-util](https://gitlab.com/thorchain/asgardex-common/asgardex-util)\
[https://gitlab.com/thorchain/asgardex-common/asgardex-midgard](https://gitlab.com/thorchain/asgardex-common/asgardex-midgard)

### Others

Other packages are available, built by the community to help with access to THORChain.

[https://github.com/thorswap/thorchain.js](https://github.com/thorswap/thorchain.js)

[https://github.com/thorswap/midgard-sdk-v1](https://github.com/thorswap/midgard-sdk-v1)
