---
description: No complex IDO, Maya will launch $CACAO with our own Liquidity Auction design!
---

# 🍫 Fair Launch

Maya has aimed to maintain its motto from the beginning: a multi-chain liquidity protocol in the hands of the community, protected by code and open to exchange. Initially, we felt that the most successful way to achieve this goal was through an Airdrop allocation, but it’s time to upgrade to something that will boost liquidity in the system even further: a Liquidity Auction.

### ELI5

1\. Different strategies are used to raise funds every time a new crypto/DeFi project is born. Some models might be better, but that depends on the team’s needs and creativity. There are many different ways in which DeFi projects can distribute their tokens to their users or community. Some examples include holding public sales —2017 ICOs are the classic example— Airdrops, farm rewards, and more.&#x20;

2\. Maya Protocol’s token distribution will work using a Liquidity Auction with the following cool pros:&#x20;

* &#x20;Lots of transparency – everybody knows when everything is happening and how.&#x20;
* &#x20;Permissionless – anybody can participate, and there are no prohibitive minimum amounts or whitelists. &#x20;
* Reduced volatility – there is the symmetry of information. No one is excluded or earns less because they participated later.

3\. “Liquidity Auction” sounds sophisticated, but it is actually straightforward:

A) Anybody can contribute supported assets, such as $BTC, $USDC, $ETH, $USDT, and even $RUNE, to the auction during a 21-day timeframe by sending them to a specified address. No KYC or registration of any kind is required except creating a Maya wallet beforehand (User Interfaces can do this for you). Also, no swaps will be allowed during this period, only adding and withdrawing liquidity! —More on the Liquidity Auction Tiers later. &#x20;

<figure><img src="../../.gitbook/assets/WHITE PAPER GRAPHICS -26.png" alt=""><figcaption></figcaption></figure>

B) After the auction, 90% of the $CACAO tokens are distributed to the participants proportional to their liquidity contributions, and the remaining 10% goes to the ILP reserve. For example: if $BTC is 40% of the liquidity raised, that pool receives 40% of the $CACAO allocation.&#x20;

<figure><img src="../../.gitbook/assets/frasquitos.png" alt=""><figcaption></figcaption></figure>

C) That’s it! Participants become Liquidity Providers by having their contributed assets + their new $CACAO tokens deposited inside Maya’s pools, facilitating swaps to other users and earning a share of the fees generated.

<figure><img src="../../.gitbook/assets/WHITE PAPER grafico .png" alt=""><figcaption></figcaption></figure>

&#x20;

\
