# Economic overview FL

Under normal operational conditions (after the Liquidity Auction finishes), all of Maya’s AMM pools will have a 1:1 ratio between native assets and $CACAO, which means that anybody wanting to participate in the protocol would ideally have to match their native asset contributions with the same amount denominated in $CACAO tokens; this is called “symmetric liquidity”.&#x20;

If, for some reason, we would want to add only one of the two assets —“asymmetric liquidity”— a slip fee would be charged because imbalances would be generated within the liquidity pool.&#x20;

During the Liquidity Auction, all the external liquidity provided will be asymmetric because nobody has had the possibility of buying $CACAO yet —it virtually does not exist yet! Particularly interesting is that users can participate in the Auction by contributing $RUNE into our $RUNE / $CACAO pool and the effects that this pool will have for the Maya <> THORChain interconnection, presenting many arbitrage opportunities and inviting traders and bots to bridge between the two protocols continuously to take advantage of them (the first step in our vision of a network of Layer Zeroes is becoming price leaders in the crypto market!).&#x20;

It is important to mention that because $CACAO is a native coin to a CosmosSDK blockchain, it would be very easy to integrate into any wallet or exchange that can already handle $RUNE, $LUNA, $ATOM, $OSMO, and many others. $CACAO enjoys the rest of the ecosystem's advantages as well, such as cheap transaction costs, fast settlement times (<10s), ease of use, and secure wallet/transaction systems. Any exchange that wished to list our coin would be able to do so quickly and easily.

### **Liquidity Auction Withdrawals Tiers**

To prevent rapid withdrawals/dumps right after the $CACAO distribution and subsequent lockup period ends, the concept of Liquidity Auction Withdrawals Tiers has been introduced.

With this model, liquidity providers with longer-term horizons are slightly rewarded at the expense of shorter-term players, and the protocol gets some extra time to gain traction and reach stability. These Tiers work like so:

**Tier 1**&#x20;

<mark style="color:blue;">Lockup Period:</mark> 200 days&#x20;

<mark style="color:blue;">Daily withdrawal limit:</mark> Up to 0.5%

You declare your intent by adding a :1 to the Add Liquidity transaction. Frontends will abstract this away.

After the LA finishes and $CACAO is distributed, Tier 1 LPs receive a portion of all ceded $LP units, which means they can end up with an effective return of 2x or more on their contributed assets (assuming constant prices).

Importantly, Tier 1 LPs also receive an allocation of the $MAYA token. Please refer to [Part 2](../usdmaya-token/) of this Whitepaper for more details.

Tier 1 liquidity will not be able to be withdrawn during the length of the auction.

**Tier 2**&#x20;

<mark style="color:blue;">Lockup Period:</mark> 90 days&#x20;

<mark style="color:blue;">Daily withdrawal limit:</mark> Up to 1.5%

You declare your intent by adding a :2 to the Add Liquidity transaction. Frontends will abstract this away.

After the LA finishes and $CACAO is distributed, Tier 2 LPs cede 10% of their total LP units, which means they end up with an effective return of 1.8x on their contributed assets (assuming constant prices).

**Tier 3**&#x20;

<mark style="color:blue;">Lockup Period:</mark> 30 days&#x20;

<mark style="color:blue;">Daily withdrawal limit:</mark> Up to 4.5%

You declare your intent by just adding liquidity.

After the LA finishes and $CACAO is distributed, Tier 3 LPs cede 33% of their total LP units, which means they end up with an effective return of 1.34x on their contributed assets (assuming constant prices).

<figure><img src="../../.gitbook/assets/Tabla Maya.png" alt=""><figcaption></figcaption></figure>

**Risks and Lockup Period Opt-out**

* Warning! If the LA is deemed disadvantageous for any reason (for example, there was too little liquidity raised) and the community decides to undo everything via [Ragnarok](https://docs.thorchain.org/network/governance#emergency-changes), Tier 2 and 3 LPs might end up receiving back less than they originally deposited and effectively take a loss.&#x20;
* LPs that prefer not to have these types of risks or withdrawal limits can wait for the Liquidity Auction to be over and manually acquire $CACAO from a pool to then add liquidity. Wallets funded this way will have no limits or lockups.

**Note:** The best strategy for aspiring nodes is to participate as a Tier 1 LP, given that they theoretically have a long-term commitment already. This would mean a better shot at having more Liquidity for potential Liquidity Bond Wars.
