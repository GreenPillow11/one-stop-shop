# Technical overview LN

### User stories: &#x20;

1\. LP bonding&#x20;

* As a node operator, I want to be able to use added liquidity as a bond by providing a node address in the bond message and signing the message with the liquidity provider address so that the network can take advantage of the bond to be part of the liquidity. The Node address and LP address are one-to-one.
* As a liquidity node, I want withdrawals to be disabled for the bonded liquidity so that any node operating has a stake in the network.&#x20;

2\. AntiLP slashing&#x20;

* As a Node Operator, I want other nodes slashed when they don’t vote, have downtime or misbehave. This slash is in the form of LP slash points, or Anti-LP Units, that will later be settled by the slashed node at withdrawal.&#x20;
* As a node operator, I want to forgive LP Slash points with the following format FORGIVE:\[Asset]:\[Amount]:\[Address (optional)] with 67% consensus so that systemic network problems don't affect node funds and security.&#x20;
* As a node operator, I want LP Slash points owed by the 1st  quartile Node automatically forgiven every 120 blocks so that most slash points given by network errors are negligible, and  Slash is mostly for considerable downtime and misbehavior.&#x20;
* As a node operator, I want other Node Operators forcefully removed from the validator set if their AntiLPT tokens become  20% of their bonded liquidity so that they do not represent a  security risk for the network.&#x20;

3\. Incentive Curve&#x20;

* As a liquidity node, I want to receive both node-exclusive rewards AND liquidity awards according to the Incentive Curve model so that I can cover the operating expenses of running a node.&#x20;
* As a liquidity provider, I want to receive liquidity awards according to the Incentive Curve model so that the network always remains safe.

4\. Fair Launch Consideration&#x20;

* As a genesis node, I want to set a Mimir Key that overrides the  Bonded Liquidity / Total Liquidity parameter to 85% such that LPs do get rewards right after the Liquidity Auction despite there being  0 bonded liquidity at that time. &#x20;
* As a genesis node, once more than 12 Nodes have churned in. I want to keep overriding Mimir Keys slowly lower over an extended period of time until the real parameter equals the overwritten parameter, ensuring the network is in a safe state before genesis nodes churn out.&#x20;
