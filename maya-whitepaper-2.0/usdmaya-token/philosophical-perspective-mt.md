# Philosophical perspective MT

Having two tokens is directly related to our decision to launch using a Liquidity Auction and to how we want our community to be as equitable, as big, and as widespread as possible.&#x20;

$MAYA’s design has been carefully planned to prevent incentives’ misalignments for the insiders holding them, and they allowed us to financially bootstrap our project in its earliest stages without having to recur to any pre-sales of $CACAO, which we really wanted to avoid.&#x20;

Both tokens can be freely traded, and they offer their respective holders the right to earn a percentage of the fees generated inside our protocol, although with different approaches, as described below.&#x20;

**On Governance.** To achieve a high level of decentralization Maya has minimal governance, similar to THORChain. This aspect is directly related to the security of the protocol so that the nodes are the ones who carry out the governance and ensure that all incentives are granted through code. In the specific case of $MAYA tokens, they don’t give any governance rights to their holders, or any other right whatsoever.&#x20;

You can see more about this topic [here](https://docs.mayaprotocol.org/how-it-works/governance). &#x20;

<figure><img src="../../.gitbook/assets/WHITE PAPER GRAPHICS -30.png" alt=""><figcaption></figcaption></figure>
