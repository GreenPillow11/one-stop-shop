# Technical overview MT

### **Who will own the $MAYA tokens?**&#x20;

**First of all, $RUNE owners**

We are a friendly fork of THORChain and have no interest in vampiring away any of their capital or any of their users. In fact, we plan on sharing 7% of the total $MAYA token supply with them to acknowledge their support for THORChain, which in return makes Maya Protocol possible.&#x20;

$RUNE owners will get $MAYA tokens freely, simply by:&#x20;

**A.** Having $RUNE bonded in a Node,

**B.** Having $RUNE locked in a symmetrical LP position,

**C.** Having a $RUNE asymmetrical LP position,

**D.** Holding $RUNE in a wallet and/or,

**E.** Holding $RUNE in Maya pools during the auction.

**Cases A and B give a 4x boost. Cases C and D have a 2x boost. Case E has a 1x boost.**

Note that asymmetric positions denominated in non-$RUNE don’t receive an airdrop since we can’t relate their Maya wallets to any Thor wallet.

Importantly, to make sure that only “fresh” capital is attracted during our launch (i.e., there is no capital leaving THORChain), we designed the following ruleset:&#x20;

**1.** Daily snapshots of $RUNE distribution on THORChain will be taken every day at random for 42 days, starting right before the Liquidity Auction and running through 21 days after the end of the Liquidity Auction.&#x20;

**2.** $MAYA tokens will be distributed considering the smallest $RUNE position that the $RUNE owners held in any of these 42 snapshots.&#x20;

This way, if, for example, whoever $RUNE holder sells half of his position to add it to our Liquidity Auction looking to get some $CACAO tokens, that holder would only get half of his $MAYA tokens at distribution.&#x20;

Ultimately, if you want a bigger share of the $MAYA tokens as an OG THORChain supporter, you are encouraged to hold your $RUNE positions or even increase them, and if you simultaneously want a bigger share of $CACAO allocation, you are encouraged to participate in the Liquidity Auction with capital brought from other, different sources.

The process’ details to create a Maya wallet that receives the corresponding $MAYA allocation as a $RUNE holder will be announced separately, but it will simply require you to create a Maya Address and send at least 1 $RUNE as Add Liquidity asymmetrically or symmetrically (which can be withdrawn during the snapshot period, we only need this transaction to relate your Maya Wallet to your THORChain Wallet).

<figure><img src="../../.gitbook/assets/WHITE PAPER GRAPHICS -32.png" alt=""><figcaption></figcaption></figure>

**Second, our Early Node operators!**&#x20;

An additional 7% of the $MAYA total supply will be used to reward our early node operators like so:

1. 1% of all the $MAYA tokens will be shared with the active Validator Nodes securing our network one month after the end of the Liquidity Auction.&#x20;
2. An additional 2 % of all the $MAYA tokens will be shared with the active Validator Nodes securing the network four months after the end of the Liquidity Auction.&#x20;
3. An additional 2 % of all the $MAYA tokens will be shared with the active Validator Nodes securing the network six months after the end of the Liquidity Auction.&#x20;
4. An additional 2% of all the $MAYA tokens will be shared with the active Validator Nodes securing the network twelve months after the end of the Liquidity Auction.&#x20;

This token incentive rewards our early heroes and supporters and potentially catalyzes our first bond wars since only churned-in nodes become eligible. While bond wars are great for THORChain, they will be even more beneficial to Maya for reasons that will be covered in [Part 3](broken-reference) of this Whitepaper.

**Third, Maya Mask owners!**

Maya Mask owners will receive 1% of the total $MAYA tokens supply! You can view the collection on [Opensea](https://opensea.io/collection/mayamasks).

**Fourth, our Tier 1 Liquidity Providers**

Another 7% of the $MAYA tokens will be distributed to our early and most timewise committed Liquidity Providers.

This allocation will be proportionally distributed in three parts for all LP positions that remain 200, 350, and 500 days after the LA $CACAO distribution.

The fewer withdrawals made as a Tier 1 LP, the larger share of $MAYA tokens received on each one of these dates.

**Last but not least, the Dev Fund**&#x20;

The remaining 78% of the tokens will be initially awarded to the Maya team at all levels of the organization, including our developers, our advisors, our investors, and other strategic individuals and institutions that have readily supported us.

<figure><img src="../../.gitbook/assets/WHITE PAPER  MAYA.png" alt=""><figcaption></figcaption></figure>

Remember, none of them own any $CACAO yet, nor will they do at any point before or after our Liquidity Auction unless they participate in it with their own funds under the same terms as any other participant. This is very positive for the Maya community since there is no counterparty risk of these participants dumping or rug pulling $CACAO since they got it at the same price basis as anyone else during the Auction. Likewise, dumping $MAYA has no direct effect on the price of $CACAO. Therefore, $CACAO will have very little sell pressure from its origins, a liability that most protocols often have. $MAYA tokens are the only way to repay them for their big-time commitment and sterling efforts in a fair way that is aligned with the community.&#x20;

As a final display of the Maya founders’ commitment for the long run, their own share of $MAYA tokens will not be transferable and will be permanently locked in perpetuity. These tokens will only accrue $CACAO fees over time, which will be transferable.&#x20;

Thorstarter’s share of $MAYA tokens will be shared with Forge Stakers. Thorstarter will announce separately how their supporters can benefit from our launch.&#x20;

### User Stories:&#x20;

1\. Maya Fund

As a Maya user, I want the Maya Fund to be funded with 10% of the gas and swap fees that are generated in the chain to distribute those funds among the Maya users that hold $MAYA Token, denominated in $CACAO.&#x20;

As a Maya user holding $MAYA, I want to receive $CACAO distributions from the Maya Fund proportional to the amount of $MAYA Tokens I own every 14,400 blocks (approximately every 24 hours).
