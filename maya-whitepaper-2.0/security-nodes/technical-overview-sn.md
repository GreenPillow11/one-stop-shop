# Technical overview SN

### User Stories: &#x20;

1. Node Whitelist&#x20;

* As a Maya node operator, I should be able to register a Chain B  validator address and set its public key as an attribute of my Maya node.
* As a node operator, I require that nodes of Chain B can only become validators if they have an active validator in Maya.&#x20;
* As a node operator, I require that nodes of Chain B that have been churned out of Maya are also churned out from Chain B.&#x20;
* As a node operator, I require that the Validator Node Set in Chain B be only 80% the size of the Maya Node Set, essentially a subset of Maya Nodes.
* As a node operator, I require that nodes compete on pure $CACAO bonds in Chain B to be part of the Chain B Node Set.&#x20;

2\. IBC&#x20;

* As a user of Chain B, I want to be able to change tokens from one chain to another securely so that I can use $CACAO as a native token in each of them.&#x20;
* As a user of Chain B, I want to pay fees in $CACAO and have Chain B governance dependent on $CACAO.&#x20;

3\. Treasuries&#x20;

* As a Maya node, I want a treasury to exist in Maya that can be made to do automatic coded actions and allocate capital by supermajority node vote at will.&#x20;
* As a Chain B node, I want a treasury to exist in Chain B that can be made to do automatic coded actions and allocate capital by supermajority node vote at will. &#x20;

4\. Taxation&#x20;

* As a Maya node, I want the treasury to collect fees from expatriation and repatriation of $CACAO. &#x20;
* As a Maya node, I want to be able to tweak Max Debt as well as taxation constants for Chain B through a validator node supermajority vote. &#x20;
