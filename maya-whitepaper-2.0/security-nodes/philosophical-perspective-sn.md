# Philosophical perspective SN

To help bring the decentralization revolution to the masses, a network of financial, contractual, entertainment and utility products must exist, but it is very difficult to compound many of these functionalities into only one chain since trade-offs between security and network clogging are faced constantly; this is why we believe in a multi-chain approach.&#x20;

Some Application-Specific Blockchains (ASBCs) are powerful and useful but then lack the security and solvency to operate securely. We believe that this is the case for most of the CosmosSDK-based chains except for THORChain.

Whereas most of the Cosmos-based chains rely on weakly-bonded, doxxed nodes with delegated funds, THORChain requires nodes that bond huge amounts of their own capital, running an often over-bonded chain that remains completely anonymous.

### Enter Security Nodes

By sharing Maya nodes’ capacities with other projects and chains, we can export our security and solvency and allow for more specific applications —think trading, NFTs, stablecoins, metaverse, etc.— to integrate with us and generate additional demand for $CACAO in the process.

In other words, the nodes’ set of any Application-Specific Blockchains  (ASBCs) that would want to connect to our ecosystem would always belong to the set of Maya nodes too, which means that these side chains would be secured by nodes with huge stakes in $CACAO and that all of the involved participants would have aligned incentives to care for the stability and growth of the token. This would mean that to capture a  Maya sidechain, you would have to capture Maya itself first, which is economically unfeasible for a rational actor.&#x20;

New chains would need to bring utility and growth to the ecosystem, of course, since running them and exporting $CACAO to them would have economic costs. In this regard, they can be considered economic ventures, which may or may not succeed. There is a max limit of $CACAO token withdrawals for each one of these side chains that we call “Max Debt,” and which can be modulated by the Maya nodes’ consensus. Should one of these chains be called risky/faulty / failed, then the Max Debt variable could be reduced by our nodes slowly to repatriate the previously exported $CACAO until all $CACAO has been recalled.&#x20;

During growth cycles, if $CACAO’s price rises too much, the Max Debt variable could also be reduced to repatriate the tokens in preparation for any potential ensuing contraction cycle. Conversely, after economic headwinds, Max Debt could be slowly increased to leverage the sidechain through lower prices, boost its economic activity, and prepare for potential future growth.

\
