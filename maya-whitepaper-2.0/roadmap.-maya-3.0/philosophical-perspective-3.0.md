# Philosophical perspective 3.0

We believe in iterative learning, and we have been following the Terra / LUNA ecosystem —and subsequent implosion— for months now. In pursuing an algorithmic stablecoin system that considers depegs, death spirals, and bank run as highly probable scenarios, we designed an innovative assortment of stablecoins plus a Treasury Bond mechanism.&#x20;

These are the tenets that we looked for and applied to our design philosophy:&#x20;

1\. Decentralized Stablecoins are good for DeFi and necessary to the future of Crypto.&#x20;

2\. Decentralized Stablecoins’ design hasn't reached maturity yet. We are still in the iteration phase.&#x20;

3\. These constant iterations mean that failure is always possible, but it should not grow into a big, uncontrollable systemic risk.&#x20;

4\. Single Points of Failure must be avoided.&#x20;

5\. Inflating our native token beyond its total supply should not be an option since it is the core of our economy and security budgets.&#x20;

6\. Demand for our stablecoins should be real, solid, and deep-rooted; instead of artificial and mercenary.

7\. Fairness should drive decision-making.&#x20;

The Maya Economy will have 5 stablecoins in its suite, each one with a  different economic design plus different strengths and weaknesses; they will all help share the market’s needs and alleviate external pressures on supply and demand. Failure of one design should not trickle into the others, and systemic shocks should have orderly and reasonable responses.

<figure><img src="../../.gitbook/assets/WHITE PAPERRR .png" alt=""><figcaption></figcaption></figure>

\
