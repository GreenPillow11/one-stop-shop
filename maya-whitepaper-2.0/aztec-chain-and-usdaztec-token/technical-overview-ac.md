# Technical overview AC

## Who will own the $AZTEC tokens?&#x20;

### **First of all, $LUNA / $UST / GAIA owners!**&#x20;

Although we really did not expect the Terra / LUNA situation to unfold as it did in May 2022, we had long planned on awarding $AZTEC tokens to $LUNA holders in the same fashion we are doing with $RUNE holders. The Terra community’s situation is continuously evolving, but our ethos has always been to collaborate and be a positive force in the DeFi community.&#x20;

In the beginning, we decided to fork Terra, but the reality is that the code that could have been useful to us was no longer being maintained. Therefore we decided to develop a chain from scratch with the latest version of Cosmos SDK. That is why we decided to give **5% of the $AZTEC supply to GAIA holders.**&#x20;

Whatever we need for Aztec DeFi, we will do it ourselves. Still, we would like to honor our initial inspiration for Aztec with **2.5% to $LUNA holders and 2.5% to $UST holders** of original LUNA 1.0.

### Second, Early Nodes&#x20;

**10% of the $AZTEC total supply** will be used to reward our early node operators like so:

1. 3.33% of all the $AZTEC tokens will be shared with the active Validator Nodes securing our network 1 month after the end of the Liquidity Auction.&#x20;
2. An additional 3.33% of all the $AZTEC tokens will be shared with the active Validator Nodes securing the network 4 months after the end of the Liquidity Auction.&#x20;
3. An additional 3.33% of all the $AZTEC tokens will be shared with the active Validator Nodes securing the network 12 months after the end of the Liquidity Auction.&#x20;

This token incentive rewards our early heroes and supporters and potentially catalyzes our first bond wars since only churned-in nodes become eligible. While bond wars are great for THORChain, they will be even more beneficial to Maya for reasons covered in [Part 3](broken-reference) of this Whitepaper.

### **Last but not least, the Dev Fund**&#x20;

The remaining **79% of the tokens will be initially awarded to the Maya team** at all levels of the organization, including our developers, our advisors, our investors, and other strategic individuals and institutions that have readily supported us.

Remember, none of them own any $AZTEC yet, nor will they do at any point before or after our Liquidity Auction unless they participate in it with their own funds under the same terms as any other participant. This is very positive for the Maya community since there is no counterparty risk of these participants dumping or rug-pulling $CACAO since they got it at the same price as anyone else during the Auction. Likewise, dumping $AZTEC has no direct effect on the price of $CACAO. Therefore, $CACAO will have very little sell pressure from its origins, a liability that most protocols often have. $AZTEC tokens are the only way to repay them for their big-time commitment and sterling efforts in a fair way aligned with the community.&#x20;

As a final display of the Maya founders’ commitment for the long run, their own share of $AZTEC tokens will not be transferable and will be permanently locked in perpetuity. These tokens will only accrue $CACAO fees over time, which will be transferable.&#x20;

Thorstarter’s share of $MAYA tokens will be shared with Forge Stakers. Thorstarter will announce separately how their supporters can benefit from our launch.&#x20;

<figure><img src="../../.gitbook/assets/WHITE PAPER .png" alt=""><figcaption></figcaption></figure>

### User Stories: &#x20;

1\. Remove Delegation Logic&#x20;

* As an Aztec validator, I want to stake without delegation, so that I  have a large enough stake to incentivize good behavior. I can only be  a Validator in Aztec if I already am a validator in Maya.&#x20;

2\. Aztec Fund&#x20;

* As an $AZTEC holder, I want the Aztec Fund to be funded with 10% of  the gas and transaction fees that are generated in the chain to  distribute those funds among the Aztecs users that own $AZTEC,  denominated in $CACAO.&#x20;
* As an $AZTEC holder, I want to receive funds from the Aztec Fund  proportional to the amount of $AZTEC tokens I own, distributed  every 24 hours.&#x20;

3\. Turn off mint/burn mechanism&#x20;

* As an Aztec user, I want to use a safe protocol. Until many things are  validated about the economic design, I want all stablecoin mecha nisms turned off.
